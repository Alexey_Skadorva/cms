package by.bsu.cms.entity;

import org.hibernate.validator.constraints.NotEmpty;

public class FileData {
    @NotEmpty
    private String name;
    private String type;
    private String code;
    private String path;


    public FileData() {
    }

    public FileData(String name, String type, String path, String code) {
        this.name = name;
        this.code = code;
        this.type = type;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
