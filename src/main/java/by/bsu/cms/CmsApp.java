package by.bsu.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CmsApp {
    public static void main(String[] args) {
        SpringApplication.run(CmsApp.class, args);
    }
}
