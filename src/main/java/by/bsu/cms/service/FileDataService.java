package by.bsu.cms.service;

import by.bsu.cms.entity.FileData;
import by.bsu.cms.enums.FileType;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class FileDataService {

    private final static String FILE_ENCODING = "UTF-8";

    public List<FileData> read(String path, List<FileData> filenames) {
        File myFolder = new File(path);
        File[] files = myFolder.listFiles();
        for (File f : files) {
            if (f.isFile()) {
                if (f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1).contains(FileType.HTML.getValue()))
                    filenames.add(new FileData(f.getName(), FileType.HTML.getValue(), f.getAbsolutePath().replaceAll("\\\\", "-"), null));
                if (f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1).contains(FileType.JS.getValue()))
                    filenames.add(new FileData(f.getName(), FileType.JS.getValue(), f.getAbsolutePath().replaceAll("\\\\", "-"), null));
                if (f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1).contains(FileType.CSS.getValue()))
                    filenames.add(new FileData(f.getName(), FileType.CSS.getValue(), f.getAbsolutePath().replaceAll("\\\\", "-"), null));
            } else if (f.isDirectory()) {
                read(f.getAbsolutePath(), filenames);
            }
        }
        return filenames;
    }

    public String readFromFile(String path) throws IOException {
        File fileDir = new File(path.replaceAll("-", "\\\\"));

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileDir), FILE_ENCODING));

        final StringBuffer code = new StringBuffer();
        String line;
        while ((line = in.readLine()) != null) {
            code.append(line + "\n");
        }

        in.close();

        return code.toString();
    }

    public List<String> findDependency(String code) {
        List<String> dependency = new ArrayList<>();
        Pattern pattern = Pattern.compile("href=\\\"(.*?)\\\"");
        Matcher matcher = pattern.matcher(code);
        if (matcher.find()) {
            dependency.add(matcher.group(1));
        }
        return dependency;
    }

    public void createFile(FileData fileData) throws Exception {
        PrintWriter writer = new PrintWriter(fileData.getName() + "." + fileData.getType(), FILE_ENCODING);
        if(fileData.getType().equals(FileType.CSS.getValue()) || fileData.getType().equals(FileType.JS.getValue())){
            fileData.setCode(getCode(fileData.getCode()));
        }
        writer.println(fileData.getCode());
        writer.close();
    }

    public void deleteFile(String page) throws IOException {
        page = page.replaceAll("-", "\\\\");
        Path path = Paths.get(page);
        Files.delete(path);
    }

    public void updateFile(FileData fileData) throws IOException {
        fileData.setPath(fileData.getPath().replace("-", "\\\\"));
        File file = new File(fileData.getPath());

        Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file.getAbsoluteFile()), FILE_ENCODING));

        if(fileData.getType().equals(FileType.CSS.getValue()) || fileData.getType().equals(FileType.JS.getValue())){
            fileData.setCode(getCode(fileData.getCode()));
        }
        out.append(fileData.getCode());

        out.flush();
        out.close();

        File newName = new File(fileData.getPath().substring(0, fileData.getPath().lastIndexOf("\\") + 1) + fileData.getName() + fileData.getType());

        file.renameTo(newName);
    }

    private String getCode(String code){
        return code.substring(code.indexOf("<p>") + 3, code.indexOf("</p>"));
    }
}
