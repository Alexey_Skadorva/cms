package by.bsu.cms.controller;

import by.bsu.cms.entity.FileData;
import by.bsu.cms.enums.FileType;
import by.bsu.cms.service.FileDataService;
import by.bsu.cms.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/administration")
public class FileDataController {

    @Autowired
    private Validator validator;

    @Autowired
    FileDataService fileDataService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<FileData> fileDates = fileDataService.read(".", new ArrayList<FileData>());
        model.addAttribute("list", fileDates);
        return "list";
    }

    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String editPage(@PathVariable String page, Model model, HttpServletRequest request) throws IOException {
        String code = fileDataService.readFromFile(page);
        FileData fileData = new FileData(page.substring(page.lastIndexOf("-") + 1, page.lastIndexOf(".")),
                page.substring(page.lastIndexOf(".")), page, code);
        ControllerUtil.setReferrer(request, model);
        model.addAttribute("file", fileData);
        System.out.println(code);
        /*List<String> dependency = fileDataService.findDependency(code);
        model.addAttribute("dependency", dependency);*/
        return "update";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createPage(Model model, HttpServletRequest request) throws IOException {
        ControllerUtil.setReferrer(request, model);
        model.addAttribute("file", new FileData());
        model.addAttribute("fileTypes", FileType.values());
        return "create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@ModelAttribute(value = "file") FileData fileData, BindingResult bindingResult,
                         @RequestParam(value = "referrer") String referrer, Model model) throws Exception {
        validator.validate(fileData, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("fileTypes", FileType.values());
            model.addAttribute("referrer", referrer);
            return "create";
        }
        fileDataService.createFile(fileData);
        return "redirect:/administration/list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute(value = "file") FileData fileData, BindingResult bindingResult,
                       @RequestParam(value = "referrer") String referrer, Model model) throws Exception {
        validator.validate(fileData, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("referrer", referrer);
            return "update";
        }
        fileDataService.updateFile(fileData);
        return "redirect:/administration/list";
    }

    @RequestMapping(value = "/{page}/delete", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String delete(@PathVariable String page) throws IOException {
        fileDataService.deleteFile(page);
        return "redirect:/administration/list";
    }
}
