package by.bsu.cms.controller;

import by.bsu.cms.util.ControllerUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LogoutController {
    @RequestMapping(value = "/logoutDialog", method = RequestMethod.GET)
    public String logoutDialog(Model model, HttpServletRequest request) {
        ControllerUtil.setReferrer(request, model);
        return "logout";
    }
}
