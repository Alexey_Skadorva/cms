package by.bsu.cms.enums;

public enum FileType {
    HTML("html"), JS("js"), CSS("css");

    private String value;

    public String getValue() {
        return value;
    }

    FileType(String value) {
        this.value = value;
    }
}
