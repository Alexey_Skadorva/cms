$(document).ready(function () {

    $("select:visible").selectric();

    $("form").submit(function() {
        window.onbeforeunload = null;
    });

    var confirmLeave = function() {
        window.onbeforeunload = function() {
            return "Вы действительно хотите покинуть эту страницу?";
        };
    };

    $("input, select, textarea").on("input", function() {
        confirmLeave();
    });

    $("input[type=hidden], select").on("change", function () {
        confirmLeave();
    });
});