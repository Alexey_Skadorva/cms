$(function () {

    $(".delete").click(function(e) {
        e.preventDefault();

        var $dialog = $("#confirmationDialog"),
            deleteUrl = $(this).attr("data-url");

        $dialog.find("#confirmationMessage p.message")
            .text("Вы действительно хотите удалить выбранную запись?");

        $dialog.find("button.confirm").text("Подтвердить");

        $(".overlay, #confirmationDialog .top .close, #confirmationDialog button.cancel").unbind().click(function (e) {
            e.preventDefault();
            $dialog.hide();
            $("body").removeClass("body-popup");
        });

        $dialog.find("button.confirm").unbind().click(function () {
            $dialog.hide();
            $("body").removeClass("body-popup");

            $.post(deleteUrl)
                .done(function() {
                    window.onbeforeunload = null;
                    location.reload();
                })
                .fail(function() {
                    var $errorDialog = $("#errorDialog");

                    $errorDialog.find("#errorMessage p.message")
                        .text("При удалении произошла ошибка. Возможно, запись уже используется.");

                    $(".overlay, #errorDialog .top .close, #errorDialog button.cancel").unbind().click(function (e) {
                        e.preventDefault();
                        $errorDialog.hide();
                        $("body").removeClass("body-popup");
                    });

                    $("body").addClass("body-popup");
                    $errorDialog.show();
                })
                .always(function() {

                });
        });

        $("body").addClass("body-popup");
        $dialog.show();
    });

});
